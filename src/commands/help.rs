use super::Command;

pub struct Help;

impl Command for Help
{
    fn num_args(&self) -> std::ops::RangeInclusive<usize> { todo!() }

    fn execute(
        &self,
        ratings: &mut underground_analyse::MixesElo<underground_analyse::sql_db::SQLDb>,
        args: &[&str],
    ) -> Result<(), <underground_analyse::sql_db::SQLDb as underground_analyse::Database>::Error>
    {
        todo!()
    }

    fn help(&self) -> &'static str { todo!() }
}
